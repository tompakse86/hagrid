#
# Translators:
# Vincent Breitmoser <look@my.amazin.horse>, 2020
# ylsun <y15un@y15un.dog>, 2021
#
msgid ""
msgstr ""
"Project-Id-Version: hagrid\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-06-15 16:33-0700\n"
"PO-Revision-Date: 2019-09-27 18:05+0000\n"
"Last-Translator: ylsun <y15un@y15un.dog>, 2021\n"
"Language-Team: Korean (https://www.transifex.com/otf/teams/102430/ko/)\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

msgid "Error"
msgstr "오류"

msgid "Looks like something went wrong :("
msgstr "뭔가 단단히 잘못된 모양입니다 :("

msgid "Error message: {{ internal_error }}"
msgstr "오류 내용: {{ internal_error }}"

msgid "There was an error with your request:"
msgstr "내 요청과 관련해 아래와 같은 문제가 발생했습니다:"

msgid "We found an entry for <span class=\"email\">{{ query }}</span>:"
msgstr "<span class=\"email\">{{ query }}</span>에 해당하는 키를 찾았습니다:"

msgid ""
"<strong>Hint:</strong> It's more convenient to use <span class=\"brand"
"\">keys.openpgp.org</span> from your OpenPGP software.<br /> Take a look at "
"our <a href=\"/about/usage\">usage guide</a> for details."
msgstr ""
"<strong>알아두면 좋아요:</strong> <span class=\"brand\">keys.openpgp.org</"
"span> 이용은 내 OpenPGP 소프트웨어를 통해서 하는 게 더 편하답니다.<br /> 자세"
"한 사항은 <a href=\"/about/usage\">사용 안내서</a>에서 알아보세요."

msgid "debug info"
msgstr "디버그 정보"

msgid "Search by Email Address / Key ID / Fingerprint"
msgstr "전자 메일 주소 / 키 ID / 지문으로 키를 찾아보세요"

msgid "Search"
msgstr "찾기"

msgid ""
"You can also <a href=\"/upload\">upload</a> or <a href=\"/manage\">manage</"
"a> your key."
msgstr ""
"내 키를 <a href=\"/upload\">올리</a>거나 <a href=\"/manage\">관리</a>할 수도 "
"있습니다."

msgid "Find out more <a href=\"/about\">about this service</a>."
msgstr "이 서비스에 대해 <a href=\"/about\">더 알아볼래요</a>."

msgid "News:"
msgstr "새 소식:"

msgid ""
"<a href=\"/about/news#2019-11-12-celebrating-100k\">Celebrating 100.000 "
"verified addresses! 📈</a> (2019-11-12)"
msgstr ""
"<a href=\"/about/news#2019-11-12-celebrating-100k\">주소 인증 100,000건을 달"
"성했습니다! 📈</a> (2019-11-12)"

msgid "v{{ version }} built from"
msgstr "버전 {{ version }} / 빌드 커밋"

msgid "Powered by <a href=\"https://sequoia-pgp.org\">Sequoia-PGP</a>"
msgstr "<a href=\"https://sequoia-pgp.org\">Sequoia-PGP</a> 기반"

msgid ""
"Background image retrieved from <a href=\"https://www.toptal.com/designers/"
"subtlepatterns/subtle-grey/\">Subtle Patterns</a> under CC BY-SA 3.0"
msgstr ""
"배경 그림은 <a href=\"https://www.toptal.com/designers/subtlepatterns/subtle-"
"grey/\">Subtle Patterns</a>에서 가져왔고 CC BY-SA 3.0 허가서를 따릅니다"

msgid "Maintenance Mode"
msgstr "유지보수 모드"

msgid "Manage your key"
msgstr "내 키 관리"

msgid "Enter any verified email address for your key"
msgstr "내 키에 대해 인증된 전자 메일 주소를 입력하세요"

msgid "Send link"
msgstr "링크 보내기"

msgid ""
"We will send you an email with a link you can use to remove any of your "
"email addresses from search."
msgstr ""
"내 전자 메일 주소를 검색에서 지울 수 있는 링크가 담긴 전자 메일 한 통이 갈 거"
"예요."

msgid ""
"Managing the key <span class=\"fingerprint\"><a href=\"{{ key_link }}\" "
"target=\"_blank\">{{ key_fpr }}</a></span>."
msgstr ""
"<span class=\"fingerprint\"><a href=\"{{ key_link }}\" target=\"_blank"
"\">{{ key_fpr }}</a></span> 키를 관리합니다."

msgid "Your key is published with the following identity information:"
msgstr "내 키에 대해 현재 아래 명의가 함께 공개돼 있습니다:"

msgid "Delete"
msgstr "지우기"

msgid ""
"Clicking \"delete\" on any address will remove it from this key. It will no "
"longer appear in a search.<br /> To add another address, <a href=\"/upload"
"\">upload</a> the key again."
msgstr ""
"\"지우기\"를 누르면 해당 주소가 내 키에서 지워지고 검색에 나타나지 않습니다."
"<br />다른 주소를 추가하려면 이 키를 다시 <a href=\"/upload\">올리세요</a>."

msgid ""
"Your key is published as only non-identity information.  (<a href=\"/about\" "
"target=\"_blank\">What does this mean?</a>)"
msgstr ""
"내 키의 비-명의 정보만 공개된 상태입니다. (<a href=\"/about\" target=\"_blank"
"\">이게 뭘 뜻하나요?</a>)"

msgid "To add an address, <a href=\"/upload\">upload</a> the key again."
msgstr "새 주소를 추가하려면 이 키를 다시 <a href=\"/upload\">올리세요</a>."

msgid ""
"We have sent an email with further instructions to <span class=\"email"
"\">{{ address }}</span>."
msgstr ""
"상세 사용 설명이 담긴 전자 메일을 <span class=\"email\">{{ address }}</span> "
"주소로 보냈습니다."

msgid "This address has already been verified."
msgstr "이 주소는 이미 인증됐습니다."

msgid ""
"Your key <span class=\"fingerprint\">{{ key_fpr }}</span> is now published "
"for the identity <a href=\"{{userid_link}}\" target=\"_blank\"><span class="
"\"email\">{{ userid }}</span></a>."
msgstr ""
"이제 <a href=\"{{userid_link}}\" target=\"_blank\"><span class=\"email"
"\">{{ userid }}</span></a> 명의가 내 <span class=\"fingerprint"
"\">{{ key_fpr }}</span> 키의 일부로 공개됩니다."

msgid "Upload your key"
msgstr "내 키 올리기"

msgid "Upload"
msgstr "올리기"

msgid ""
"Need more info? Check our <a target=\"_blank\" href=\"/about\">intro</a> and "
"<a target=\"_blank\" href=\"/about/usage\">usage guide</a>."
msgstr ""
"도움이 더 필요한가요? <a target=\"_blank\" href=\"/about\">서비스 소개</a>와 "
"<a target=\"_blank\" href=\"/about/usage\">사용 안내서</a>를 읽어보세요."

msgid ""
"You uploaded the key <span class=\"fingerprint\"><a href=\"{{ key_link }}\" "
"target=\"_blank\">{{ key_fpr }}</a></span>."
msgstr ""
"<span class=\"fingerprint\"><a href=\"{{ key_link }}\" target=\"_blank"
"\">{{ key_fpr }}</a></span> 키를 올렸습니다."

msgid "This key is revoked."
msgstr "이 키는 폐기됐습니다."

msgid ""
"It is published without identity information and can't be made available for "
"search by email address (<a href=\"/about\" target=\"_blank\">what does this "
"mean?</a>)."
msgstr ""
"명의 정보가 없는 상태로 공개됐기 때문에 이 키는 전자 메일 주소로 찾을 수 없습"
"니다. (<a href=\"/about\" target=\"_blank\">이게 뭘 뜻하나요?</a>)"

msgid ""
"This key is now published with the following identity information (<a href="
"\"/about\" target=\"_blank\">what does this mean?</a>):"
msgstr ""
"이제 다음 명의가 이 키의 일부로 함께 공개됩니다: (<a href=\"/about\" target="
"\"_blank\">이게 뭘 뜻하나요?</a>)"

msgid "Published"
msgstr "공개됨"

msgid ""
"This key is now published with only non-identity information. (<a href=\"/"
"about\" target=\"_blank\">What does this mean?</a>)"
msgstr ""
"이 키는 이제 비-명의 정보만 공개됩니다. (<a href=\"/about\" target=\"_blank"
"\">이게 뭘 뜻하나요?</a>)"

msgid ""
"To make the key available for search by email address, you can verify it "
"belongs to you:"
msgstr ""
"이 키를 전자 메일 주소로 찾을 수 있게 하려면 내가 진짜 이 주소를 가지고 있음"
"을 인증하세요:"

msgid "Verification Pending"
msgstr "인증 대기 중"

msgid ""
"<strong>Note:</strong> Some providers delay emails for up to 15 minutes to "
"prevent spam. Please be patient."
msgstr ""
"<strong>알아두기:</strong> 몇몇 서비스 제공자는 스팸 방지를 위해 최대 15분까"
"지 전자 메일 발송을 미루기도 합니다. 조금만 여유로이 기다려볼까요?"

msgid "Send Verification Email"
msgstr "인증 전자 메일 보내기"

msgid ""
"This key contains one identity that could not be parsed as an email address."
"<br /> This identity can't be published on <span class=\"brand\">keys."
"openpgp.org</span>.  (<a href=\"/about/faq#non-email-uids\" target=\"_blank"
"\">Why?</a>)"
msgstr ""
"이 키에는 전자 메일 주소가 아닌 것으로 보이는 명의 하나가 포함돼 있네요.<br /"
">이런 명의는 <span class=\"brand\">keys.openpgp.org</span>에 공개할 수 없습니"
"다. (<a href=\"/about/faq#non-email-uids\" target=\"_blank\">왜죠?</a>)"

msgid ""
"This key contains {{ count_unparsed }} identities that could not be parsed "
"as an email address.<br /> These identities can't be published on <span "
"class=\"brand\">keys.openpgp.org</span>.  (<a href=\"/about/faq#non-email-"
"uids\" target=\"_blank\">Why?</a>)"
msgstr ""
"이 키에는 전자 메일 주소가 아닌 것으로 보이는 명의 {{ count_unparsed }} 개가 "
"포함돼 있네요.<br />이런 명의는 <span class=\"brand\">keys.openpgp.org</span>"
"에 공개할 수 없습니다. (<a href=\"/about/faq#non-email-uids\" target=\"_blank"
"\">왜죠?</a>)"

msgid ""
"This key contains one revoked identity, which is not published. (<a href=\"/"
"about/faq#revoked-uids\" target=\"_blank\">Why?</a>)"
msgstr ""
"이 키에는 폐기된 명의 하나가 포함돼 있네요. 폐기된 명의는 공개하지 않습니다. "
"(<a href=\"/about/faq#revoked-uids\" target=\"_blank\">왜죠?</a>)"

msgid ""
"This key contains {{ count_revoked }} revoked identities, which are not "
"published. (<a href=\"/about/faq#revoked-uids\" target=\"_blank\">Why?</a>)"
msgstr ""
"이 키에는 폐기된 명의 {{ count_revoked }} 개가 포함돼 있네요. 폐기된 명의는 "
"공개하지 않습니다. (<a href=\"/about/faq#revoked-uids\" target=\"_blank\">왜"
"죠?</a>)"

msgid "Your keys have been successfully uploaded:"
msgstr "내 키를 성공적으로 올렸습니다:"

msgid ""
"<strong>Note:</strong> To make keys searchable by email address, you must "
"upload them individually."
msgstr ""
"<strong>알아두기:</strong> 개별 키를 전자 메일 주소로 찾을 수 있게 하려면 각"
"각 따로 올리세요."

msgid "Verifying your email address…"
msgstr "전자 메일 주소를 인증하는 중..."

msgid ""
"If the process doesn't complete after a few seconds, please <input type="
"\"submit\" class=\"textbutton\" value=\"click here\" />."
msgstr ""
"몇 초가 지나도 이 절차가 끝나지 않는다면 <input type=\"submit\" class="
"\"textbutton\" value=\"여기를 누르세요\" />."

msgid "Manage your key on {{domain}}"
msgstr "{{domain}}에 올린 내 키 관리"

msgid "Hi,"
msgstr "안녕하세요."

msgid ""
"This is an automated message from <a href=\"{{base_uri}}\" style=\"text-"
"decoration:none; color: #333\">{{domain}}</a>."
msgstr ""
"이 메시지는 <a href=\"{{base_uri}}\" style=\"text-decoration:none; color: "
"#333\">{{domain}}</a>에서 자동으로 발송됐습니다."

msgid "If you didn't request this message, please ignore it."
msgstr "만약 직접 요청하신 게 아니라면 그냥 무시하세요."

msgid "OpenPGP key: <tt>{{primary_fp}}</tt>"
msgstr "OpenPGP 키: <tt>{{primary_fp}}</tt>"

msgid ""
"To manage and delete listed addresses on this key, please follow the link "
"below:"
msgstr "위 키에 엮인 주소를 숨기는 등의 키 관리는 아래 링크에서 할 수 있어요:"

msgid ""
"You can find more info at <a href=\"{{base_uri}}/about\">{{domain}}/about</"
"a>."
msgstr ""
"더 자세한 사항은 <a href=\"{{base_uri}}/about\">{{domain}}/about</a> 페이지"
"를 참고하세요."

msgid "distributing OpenPGP keys since 2019"
msgstr "2019년 이래 OpenPGP 키 배포에 이바지하는 중"

msgid "This is an automated message from {{domain}}."
msgstr "이 메시지는 {{domain}}에서 자동으로 발송됐습니다."

msgid "OpenPGP key: {{primary_fp}}"
msgstr "OpenPGP 키: {{primary_fp}}"

msgid "You can find more info at {{base_uri}}/about"
msgstr "더 자세한 사항은 {{base_uri}}/about 페이지를 참고하세요."

msgid "Verify {{userid}} for your key on {{domain}}"
msgstr "{{domain}}에 {{userid}} 명의로 올린 키 인증"

msgid ""
"To let others find this key from your email address \"<a rel=\"nofollow\" "
"href=\"#\" style=\"text-decoration:none; color: #333\">{{userid}}</a>\", "
"please click the link below:"
msgstr ""
"다른 사람들이 내 전자 메일 주소(<a rel=\"nofollow\" href=\"#\" style=\"text-"
"decoration:none; color: #333\">{{userid}}</a>)로 위 키를 찾을 수 있게 하려면 "
"아래 링크를 따라가세요:"

msgid ""
"To let others find this key from your email address \"{{userid}}\",\n"
"please follow the link below:"
msgstr ""
"다른 사람들이 내 전자 메일 주소({{userid}})로 위 키를 찾을 수 있게 하려면\n"
"아래 링크를 따라가세요:"

msgid "No key found for fingerprint {}"
msgstr "지문 {}에 해당하는 키를 찾지 못했습니다."

msgid "No key found for key id {}"
msgstr "키 ID {}에 해당하는 키를 찾지 못했습니다."

msgid "No key found for email address {}"
msgstr "전자 메일 주소 {}에 해당하는 키를 찾지 못했습니다."

msgid "Search by Short Key ID is not supported."
msgstr "'짧은 키 ID' 형식은 지원하지 않습니다."

msgid "Invalid search query."
msgstr "올바르지 않은 검색어입니다."

msgctxt "Subject for verification email, {0} = userid, {1} = keyserver domain"
msgid "Verify {0} for your key on {1}"
msgstr "{1}에 {0} 명의로 올린 키 인증"

msgctxt "Subject for manage email, {} = keyserver domain"
msgid "Manage your key on {}"
msgstr "{}에 올린 내 키 관리"

msgid "This link is invalid or expired"
msgstr "링크가 올바르지 않거나 이미 만료됐습니다"

#, fuzzy
msgid "Malformed address: {}"
msgstr "잘못된 형식의 주소: {address}"

#, fuzzy
msgid "No key for address: {}"
msgstr "다음 주소와 엮인 키 없음: {address}"

msgid "A request has already been sent for this address recently."
msgstr "최근에 이 주소로 이미 요청이 접수됐습니다."

msgid "Parsing of key data failed."
msgstr "키 데이터를 해석하지 못했습니다."

msgid "Whoops, please don't upload secret keys!"
msgstr "아이고 맙소사! 비밀키는 올리면 안 돼요!"

msgid "No key uploaded."
msgstr "아무 키도 올리지 않았습니다."

msgid "Error processing uploaded key."
msgstr "올린 키를 처리하지 못했습니다."

msgid "Upload session expired. Please try again."
msgstr "올리기 세션이 만료됐습니다. 다시 해보세요."

msgid "Invalid verification link."
msgstr "올바르지 않은 인증 링크"
